const express = require('express');
const mongoose = require('mongoose');
const bodyParser = require('body-parser');
const cors = require('cors');
const jwt = require('express-jwt');
const registerController = require('./controllers/registrationController');
const loginController = require('./controllers/loginController');

var secret = "!@#DWe$%^gge&&**";

const app=express();




app.use(cors());
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({limit:'50mb',extended:true}))


mongoose.connect('mongodb://localhost/sportsmeet', {
    useNewUrlParser: true,
    useUnifiedTopology: true
},(err,success)=>{
    console.log("connection successfully created");
});


app.use('/register', registerController);
app.use('/login', loginController);

app.listen(6000,()=>{
    console.log('server started working')
})