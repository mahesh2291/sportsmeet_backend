const Q = require('q');
const Schema = require('./../schemas/userSchemas');
const Bcrypt = require('bcryptjs')
Register = (credentials) => {
  let defer = Q.defer();
  credentials.password = Bcrypt.hashSync(credentials.password, 10);
  let register = new Schema(credentials);
  register.save((err, res) => {
    if(err){
      defer.reject(err)
    }
    defer.resolve(res)
  })

  return defer.promise;
}

module.exports = { Register };